#!/bin/bash

__dir=$(dirname $(realpath $0))
ENV_FILE=$__dir/environment
container=mobilizon
image=framasoft/mobilizon

podman run \
    --env-file=$ENV_FILE \
    --volume=mobilizon_uploads:/var/lib/mobilizon/uploads \
    --volume=$__dir/config.exs:/etc/mobilizon/config.exs \
    --name=$container \
    --rm \
    --pod mobilizon-pod \
    $image
