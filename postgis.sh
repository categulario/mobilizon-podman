#!/bin/bash

__dir=$(dirname $(realpath $0))
ENV_FILE=$__dir/environment
container=mobilizon-postgis
image=postgis/postgis:13-3.1

podman run \
    --env-file=$ENV_FILE \
    --volume=mobilizon_db:/var/lib/postgresql/data \
    --name=$container \
    --rm \
    --pod mobilizon-pod \
    $image
