#!/bin/bash

podman pod create \
    --replace \
    --publish 4000:4000 \
    --publish 5432:5432 \
    --name mobilizon-pod
