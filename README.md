# Mobilizon in podman

## Commands

To create a new user ([docs](https://docs.joinmobilizon.org/administration/CLI%20tasks/manage_users/#create-a-new-user)):

    podman exec -it mobilizon mobilizon_ctl users.new categulario@gmail.com --password 123456 --admin --profile-username categulario

To delete all volumes:

    podman volume ls -q | grep mobi | xargs podman volume rm
